val catsVersion = "2.0.0"
val simulacrumVersion = "1.0.0"
val mouseVersion = catsVersion match {
  case "2.0.0" => "0.23"
  case _ => "0.24"
}

val cats = "org.typelevel" %% "cats-core" % catsVersion
val simulacrum = "org.typelevel" %% "simulacrum" % simulacrumVersion
val kittens = "org.typelevel" %% "kittens" % catsVersion
val mouse = "org.typelevel" %% "mouse" % mouseVersion


lazy val commonSettings = Seq (
  autoCompilerPlugins := true,
  scalaVersion := "2.12.10",
  scalacOptions += "-Ypartial-unification",
  addCompilerPlugin("org.scalamacros" % "paradise" % "2.1.0" cross CrossVersion.full)
)


lazy val util = (project in file("util"))
  .settings(
    commonSettings,
    libraryDependencies ++= Seq(
      simulacrum,
      cats,
      kittens,
      mouse
    )
  )


lazy val interface = (project in file("interface"))
  .dependsOn(util)
  .settings(
    commonSettings,
    libraryDependencies ++= Seq(
      simulacrum,
      cats
    )
  )

// lazy val array = (project in file("impl/array"))
//   .dependsOn(interface)
//   .settings()

// lazy val breeze = (project in file("impl/breeze"))
//   .dependsOn(interface)
//   .settings(
//       libraryDependencies ++= Seq(
//           "org.scalanlp" %% "breeze" % "0.12"
//       )
//   )
