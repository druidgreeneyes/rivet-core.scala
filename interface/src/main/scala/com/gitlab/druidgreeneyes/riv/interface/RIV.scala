package com.gitlab.druidgreeneyes.riv.interface

import com.gitlab.druidgreeneyes.riv.util._
import simulacrum._
import cats._
import mouse.all._
import scala.collection.mutable
import scala.collection.immutable.SortedSet


@typeclass trait RIV[T] extends HasMapping[T] {
  override type KeyType = Int
  override type ValueType = Double

  def apply(width: Int, keys: Keys, values: Values): T

  def apply(width: Int, points: Points) : T

  def empty(width: Int) : T

  def width(riv: T) : Int

  def normalize(riv: T) : T
}

object RIV {
  implicit def addSubRiv[T](implicit rivT: RIV[T], n: Negate[T]): AddSubSelf[T] = new AddSubSelf[T] {
    override def add(a: T)(b: T): T = {
      val keys = rivT.keys _
      val get = rivT.get _
      val dPoints = keys(a).diff(keys(b)).map(k =>
        VectorElement(
          k,
          if (rivT.contains(a)(k))
            get(a)(k)
          else
            get(b)(k)
        )
      )
      val iPoints = keys(a).intersect(keys(b)).map(k =>
        VectorElement(
          k,
          get(a)(k) + get(b)(k)
        )
      )
      rivT(rivT.width(a), dPoints ++ iPoints)
    }

    override def subtract(a: T)(b: T): T =
      add(a)(n.negate(b))
  }

  implicit def negateRiv[T](implicit rivT: RIV[T]): Negate[T] = new Negate[T] {
    override def negate(riv: T): T =
      addSubRiv[T].subtract(rivT.empty(rivT.width(riv)))(riv)
  }

  implicit def multDivRiv[T](implicit rivT: RIV[T]): MultDivDouble[T] = new MultDivDouble[T] {
    override def multiply[N](a: T)(b: N)(implicit conv: N => Double): T =
      rivT(
        rivT.width(a),
        rivT.keys(a),
        rivT.values(a).map(_ * conv(b))
      )
  }

  implicit def coSimRiv[T](implicit rivT: RIV[T]): CosineSimilarity[T] = new CosineSimilarity[T] {
    override def magnitude(riv: T) : Double =
      rivT.values(riv).map(x => x * x).sum |> Math.sqrt

    override def dotProduct[B] (a: T)(b: T): Double =
      rivT.keys(a).intersect(rivT.keys(b))
        .map(k => rivT.get(a)(k) * rivT.get(b)(k))
        .reduce(_ + _)
  }

  implicit def coordVecRiv[T](implicit rivT: RIV[T]): CoordinateVector[T] = new CoordinateVector[T] {

    override def apply(width: Int, points: Points): T = rivT.apply(width, points)


    override def width(vec: T): Int = rivT.width(vec)

    override def count(vec: T): Int =
      rivT.keys(vec).length

    override def points(vec: T): com.gitlab.druidgreeneyes.riv.util.Points =
      rivT.keys(vec).zip(rivT.values(vec)).map((VectorElement.apply _).tupled)
  }

  implicit def permuteRiv[T](implicit rivT: RIV[T]): Permutable[T] = new Permutable[T] {
    override def permute(perms: Permutations)(times: Int)(t: T): T =
      rivT(
        coordVecRiv.width(t),
        perms(rivT.keys(t))(times),
        rivT.values(t)
      )
  }

  implicit def copyRiv[T](implicit rivT: RIV[T]): Copy[T] = new Copy[T] {
    override def copy(t: T): T =
      addSubRiv.add(rivT.empty(rivT.width(t)))(t)
  }

  def round(sigFigs: Int)(n: Double) = {
    val s = math pow (10, sigFigs)
    s |> (n * _) |> math.round |> (_ / s)
  }

  implicit val defaultRounder: Double => Double = round(6)
}
