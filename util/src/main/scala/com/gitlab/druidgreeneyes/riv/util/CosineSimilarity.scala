package com.gitlab.druidgreeneyes.riv.util

import simulacrum._

@typeclass trait CosineSimilarity[T] {
  def magnitude(t: T): Double

  def dotProduct[B](a: T)(b: T): Double

  def cosineSimilarity[B](a: T)(b: T): Double = {
    val mag = magnitude(a) * magnitude(b)
    if (mag == 0)
      0
    else
      dotProduct(a)(b) / mag
  }
}
