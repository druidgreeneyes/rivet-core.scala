package com.gitlab.druidgreeneyes.riv.util

import simulacrum._
import scala.util.Random
import mouse.all._

case class Permutations private (permute: Array[Int], invert: Array[Int]) {
  def apply(keys: Keys)(times: Int): Keys = times match {
    case 0 => keys
    case t if t > 0 => keys.map(k => permute(k)) |> (apply(_)(times - 1))
    case t if t < 0 => keys.map(k => invert(k)) |> (apply(_)(times + 1))
  }
}

object Permutations {
  def apply(size: Int) = {
    val range = 0 until size
    val permutation = new Random(0).shuffle[Int, IndexedSeq](range).toArray
    val inverse = range.map(permutation.indexOf(_)).toArray
    new Permutations(permutation, inverse)
  }
}

@typeclass trait Permutable[T] {
  def permute(perms: Permutations)(times: Int)(t: T): T
}
