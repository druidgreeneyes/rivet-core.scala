package com.gitlab.druidgreeneyes.riv.util

import simulacrum._

@typeclass trait Negate[T] {
  @op("-") def negate(t: T): T
}

@typeclass trait MultDivDouble[T] {
  @op("*") def multiply[N] (a: T)(b: N)(implicit conv: N => Double): T
  @op("/") def divide[N] (a: T)(b: N)(implicit conv: N => Double): T = multiply(a)(1.0 / conv(b))
}

@typeclass trait AddSubSelf[T] {
  @op("+") def add (a: T)(b: T): T
  @op("-") def subtract (a: T)(b: T): T
}

@typeclass trait AddSubDouble[T] {
  @op("+") def add[N] (a: T)(b: N)(implicit conv: N => Double) : T
  @op("-") def subtract[N] (a: T)(b: N)(implicit conv: N => Double): T = add(a)(-conv(b))
}

@typeclass trait DoubleArithmetic[T] extends AddSubDouble[T] with MultDivDouble[T] with Negate[T]
