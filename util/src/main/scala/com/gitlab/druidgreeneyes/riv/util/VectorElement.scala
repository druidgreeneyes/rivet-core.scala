package com.gitlab.druidgreeneyes.riv.util

import cats._
import mouse.all._

case class VectorElement (index: Int = 0, value: Double = 0) {
  override def equals(obj: Any): Boolean = obj match {
    case VectorElement(idx, vl)  => idx == index
    case _ => false
  }

  override def hashCode = index
}

object VectorElement {

  implicit val orderingVE: Ordering[VectorElement] = new Ordering[VectorElement] {
    override def compare(x: VectorElement, y: VectorElement): Int =
      Ordering[Int].compare(x.index, y.index)
  }

  implicit val eqVE: Eq[VectorElement] = new Eq[VectorElement] {
    override def eqv(x: VectorElement, y: VectorElement): Boolean = x.index == y.index
  }

  implicit val showVE: Show[VectorElement] = new Show[VectorElement] {
    override def show(e: VectorElement) = s"{e.index}:{e.value}"
  }

  type DE = DoubleArithmetic[VectorElement]

  implicit val arithVE: DE = new DE {
    def add[N] (a: VectorElement)(b: N)(implicit conv: N => Double) : VectorElement =
      VectorElement(a.index, a.value + conv(b))

    def multiply[N] (a: VectorElement)(b: N)(implicit conv: N => Double) : VectorElement =
      VectorElement(a.index, a.value * conv(b))

    def negate(e: VectorElement): VectorElement =
      VectorElement(e.index, -e.value)
  }

  implicit val addSubSelfVE: AddSubSelf[VectorElement] = new AddSubSelf[VectorElement] {
    override def add(a: VectorElement)(b: VectorElement): VectorElement =
      if (eqVE.eqv(a, b)) arithVE.add(a)(b.value) else a

    override def subtract(a: VectorElement)(b: VectorElement): VectorElement =
      if (eqVE.eqv(a, b)) arithVE.subtract(a)(b.value) else a
  }

  def read(s: String): VectorElement =
    s.split(":") |>
      (
        a => VectorElement(
          a(0).toInt,
          a(1).toDouble
        )
      )
}
