package com.gitlab.druidgreeneyes.riv.util

import simulacrum._
import cats._
import cats.implicits._
import mouse.all._

@typeclass trait CoordinateVector[T] {
  def apply(width: Int, points: Points): T
  def width(vec: T): Int
  def count(vec: T): Int
  def points(vec: T): Points
}

object CoordinateVector {
  def apply[T](width: Int, points: Points)(implicit cv: CoordinateVector[T]) =
    cv.apply(width, points)

  implicit def eqCV[T](implicit cv: CoordinateVector[T]): Eq[T] = new Eq[T] {
    override def eqv(x: T, y: T): Boolean =
      cv.width(x) == cv.width(y) &&
        cv.points(x) == cv.points(y)
  }

  implicit def showCV[T](implicit cv: CoordinateVector[T]): Show[T] = new Show[T] {
    override def show(t: T): String =
      cv.points(t).map(_.show).mkString("|") |>
        (st => s"${cv.width(t)}|${st}")
  }

  def read[T](s: String)(implicit cv: CoordinateVector[T]): T = {
      val widthIndex = s.indexOf("|")
      val width: Int = s.substring(0, widthIndex).toInt
      val points: Points = s.substring(widthIndex + 1).split("|").map(VectorElement.read(_))
      CoordinateVector[T](width, points)
  }
}
