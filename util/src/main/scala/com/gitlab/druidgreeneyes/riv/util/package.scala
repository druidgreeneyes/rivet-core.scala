package com.gitlab.druidgreeneyes.riv

import simulacrum._
import cats._

package object util
    extends Negate.ToNegateOps
    with MultDivDouble.ToMultDivDoubleOps
    with AddSubDouble.ToAddSubDoubleOps
    with AddSubSelf.ToAddSubSelfOps
    with DoubleArithmetic.ToDoubleArithmeticOps {
  type Points = Array[VectorElement]
  type Keys = Array[Int]
  type Values = Array[Double]

  implicit def eqArr[T](arr: Array[T])(implicit eqT: Eq[T]): Eq[Array[T]] = new Eq[Array[T]] {

    override def eqv(x: Array[T], y: Array[T]): Boolean =
      x.length == y.length &&
        0.until(x.length).forall(i => x(i) == y(i))

  }
}
