package com.gitlab.druidgreeneyes.riv.util

import simulacrum._

@typeclass trait HasKeys[T] {
  type KeyType
  def keys(t: T): Array[KeyType]
}

@typeclass trait HasValues[T] {
  type ValueType
  def values(t: T): Array[ValueType]
}

@typeclass trait Containership[T] {
  type ContainedType
  def contains(t: T)(subject: ContainedType): Boolean
}

@typeclass trait Copy[T] {
  def copy(t: T): T
}

@typeclass trait HasMapping[T] extends HasKeys[T] with HasValues[T] with Containership [T] {
  override type KeyType
  override type ValueType
  override type ContainedType = KeyType

  def get(mapping: T)(key: KeyType): ValueType
}
